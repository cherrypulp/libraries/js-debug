import assert from 'assert';
import chai from 'chai';
import Debug from '../src/Debug';

const expect = chai.expect;
const should = chai.should;

describe('Debug.js', () => {
    beforeEach(() => {});


    describe('#constructor()', () => {
        it('should return an new instance');
    });

    describe('static#getInstance()', () => {
        it('should return a default instance', () => {
            const debug = Debug.getInstance();

            expect(debug).to.be.instanceOf(Debug);
            expect(Object.keys(Debug.instances)).to.include('default');
        });

        it('should return a specific instance', () => {
            const debug = Debug.getInstance('specific');

            expect(debug).to.be.instanceOf(Debug);
            expect(Object.keys(Debug.instances)).to.include('specific');
        });
    });

    // ...
});
