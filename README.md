# Debug 

[![Scrutinizer Code Quality](https://scrutinizer-ci.com/g/cherrypulp/js-debug/badges/quality-score.png?b=master)](https://scrutinizer-ci.com/g/cherrypulp/js-debug/?branch=master)
[![Build Status](https://scrutinizer-ci.com/g/cherrypulp/js-debug/badges/build.png?b=master)](https://scrutinizer-ci.com/g/cherrypulp/js-debug/build-status/master)
[![codebeat badge](https://codebeat.co/badges/259c0447-c3b6-42bc-9ee8-da6b1e1b9794)](https://codebeat.co/projects/github-com-cherrypulp-js-debug-master)
[![GitHub issues](https://img.shields.io/github/issues/cherrypulp/js-debug)](https://github.com/cherrypulp/js-debug/issues)
[![GitHub license](https://img.shields.io/github/license/cherrypulp/js-debug)](https://github.com/cherrypulp/js-debug/blob/master/LICENSE)

## Installation

`npm install @cherrypulp/debug`


## Quick start

```javascript
import Debug from '@cherrypulp/debug';

window.debug = new Debug({level: 'log'});

// then

debug.log('---');
debug.log('Combined emphasis with **asterisks and _underscores_**.');
debug.log('Emphasis, aka italics, with *asterisks* or _underscores_.');
debug.log('Strong emphasis, aka bold, with **asterisks** or __underscores__.');
debug.log('Strikethrough uses two tildes. ~~Scratch this.~~');
debug.log('> Blockquotes are very handy in email to emulate reply text.');
debug.info('Inline `code` has `back-ticks around` it.');
```

TODO


## Versioning

Versioned using [SemVer](http://semver.org/).

## Contribution

Please raise an issue if you find any. Pull requests are welcome!

## Author

  + **Stéphan Zych** - [monkeymonk](https://github.com/monkeymonk)

## License

This project is licensed under the MIT License - see the [LICENSE](https://github.com/cherrypulp/js-debug/blob/master/LICENSE) file for details.


## TODOS

- [ ] Documentation
- [ ] Unit tests (see https://stackoverflow.com/questions/30625404/how-to-unit-test-console-output-with-mocha-on-nodejs)
- [ ] Support nested markdown (ex. "Combined emphasis with **asterisks and _underscores_**.")
- [ ] Improve regex/replacer
- [ ] Add symbols (✘, ✔, ℹ, ?, ◯, ◉, ⚠, ☐, ☑, ☒, ❤, etc) https://www.w3schools.com/charsets/ref_utf_symbols.asp
