export const formats = [
    // Horizontal rule (ex. "***" or "---")
    {
        regex: /^(-{3}|\*{3})+/g,
        replacer() {
            return `%c${'-'.repeat(80)}%c\n`;
        },
        styles() {
            return ['background: #e1e4e8; color: #e1e4e8; line-height: 4px; margin: 20px 0;', ''];
        },
    },
    // Strike (ex. "~~foo~~")
    {
        regex: /~~([^~~]+)~~/g,
        replacer(m, p1) {
            return `%c${p1}%c`;
        },
        styles() {
            return ['text-decoration: line-through;', ''];
        },
    },
    // Bold (ex. "**foo**" or "__foo__")
    {
        regex: /\*\*([^**]+)\*\*|__([^__]+)__/g,
        replacer(m, p1, p2) {
            return `%c${p1 || p2}%c`;
        },
        styles() {
            return ['font-weight: bold;', ''];
        },
    },
    // Emphasis (ex. "*foo*" or "_foo_")
    {
        regex: /\*([^*]+)\*|_([^_]+)_/g,
        replacer(m, p1, p2) {
            return `%c${p1 || p2}%c`;
        },
        styles() {
            return ['font-style: italic;', ''];
        },
    },
    // Code (ex. "`foo`")
    {
        regex: /`([^`]+)`/g,
        replacer(m, p1) {
            return `%c${p1}%c`;
        },
        styles() {
            return ['background: rgba(27, 31, 35, .05); font-size: 85%; padding: .2em .4em; border-radius: 3px;', ''];
        },
    },
    // Blockquote (ex. ">foo" or "> foo")
    {
        regex: /^>(.*)/g,
        replacer(m, p1) {
            return `%c${p1}%c\n`;
        },
        styles() {
            return ['background: #f6f8fa; border-left: .25em solid #dfe2e5; color: #6a737d; padding: 0 1em;', ''];
        },
    },
];


/**
 * Return arguments with formatted strings.
 * @param args
 * @return {Array}
 */
export function formatArgs(...args) {
    if (!hasStyleSupport()) {
        return args;
    }

    let message = '';
    const params = [];

    args
        .forEach((arg) => {
            if (typeof arg === 'string') {
                const values = formatString(arg);

                if (Array.isArray(values)) {
                    message += values.shift();
                    values.forEach((value) => params.push(value));
                } else {
                    message += '%s ';
                    params.push(arg);
                }
            } else {
                message += '%o ';
                params.push(arg);
            }
        });

    return [message.trim()].concat(params);
}

/**
 * Return a formatted string.
 * @param arg
 * @return {string[]|*}
 */
export function formatString(arg) {
    if (typeof arg !== 'string') {
        return arg;
    }

    const formatted = [];

    formats.forEach((format) => {
        const match = arg.match(format.regex);

        if (match) {
            arg = arg.replace(format.regex, format.replacer);
            match.forEach((m, index) => {
                formatted.push({
                    index,
                    styles: format.styles(match),
                });
            });
        }
    });

    const styles = formatted
        .sort((a, b) => a.index - b.index)
        .reduce((acc, format) => acc.concat(format.styles), []);

    return [arg].concat(styles);
}

/**
 * Determine if the current browser support styles in console
 * @return {boolean}
 */
export function hasStyleSupport() {
    // @TODO - determine if the current browser support styles in console
    return true;
}
