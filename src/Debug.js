/**
 * @see https://developer.mozilla.org/fr/docs/Web/API/Console
 */
import {formatArgs} from './helpers';

export default class Debug {
    /**
     * Store Debug instances.
     * @type {{}}
     */
    static instances = {};

    /**
     * Debug levels description.
     * @type {Object}
     */
    static levels = {
        off: {
            level: 0,
            methods: [],
        },
        exception: {
            level: 1,
            methods: ['exception'],
        },
        error: {
            level: 2,
            methods: ['error', 'group', 'groupCollapsed', 'groupEnd'],
        },
        warn: {
            level: 4,
            methods: ['warn'],
        },
        info: {
            level: 8,
            methods: ['info'],
        },
        log: {
            level: 16,
            methods: [
                'log', 'debug', 'time', 'timeEnd', 'profile', 'profileEnd',
                'count', 'trace', 'dir', 'dirxml', 'assert', 'table',
            ],
        },
    };

    /**
     * Retrieve instance by name or create one.
     * @param name
     * @return {*}
     */
    static getInstance(name = 'default') {
        if (typeof Debug.instances[name] === 'undefined') {
            Debug.instances[name] = new Debug();
        }

        return Debug.instances[name];
    }

    /**
     * @param options
     */
    constructor(options = {}) {
        options = {
            level: 'off', // exception, error, warn, info, log, off
            logger: console,
            ...options,
        };

        this.history = [];
        this.level = options.level;
        this.logger = options.logger;

        // add levels methods from logger to this
        Object.values(this.constructor.levels)
            .reduce((acc, level) => acc.concat(level.methods), [])
            .forEach((method) => {
                this[method] = (...args) => {
                    if (!this.isAllowedTo(method)) {
                        return;
                    }

                    args = formatArgs(...args);
                    this.history.push({
                        method,
                        args,
                    });
                    this.logger[method](...args);
                };
            });
    }

    /**
     * Helper to warn about deprecation.
     */
    deprecated(...args) {
        if (!this.isAllowedTo('warn')) {
            return;
        }

        args = formatArgs(...args);
        this.warn('[deprecated]', ...args);
    }

    /**
     * Return true if the given logger method name is allowed in the current level.
     * @param fnName
     * @return {boolean}
     */
    isAllowedTo(fnName) {
        if (this.level === 'off') {
            return false;
        }

        const fnLevel = Object.values(this.constructor.levels).find((lvl) => lvl.methods.indexOf(fnName) > -1);
        return fnLevel.level <= this.constructor.levels[this.level].level;
    }

    /**
     * Helper to log current scope parameters.
     * @example
     * function foo(bar) {
     *     log.params({group: false});
     * }
     * foo('baz'); // bar = 'baz'
     * @param options
     */
    params(options = {}) {
        if (!this.isAllowedTo('log')) {
            return;
        }

        options = {
            group: true,
            groupCollapsed: true,
            groupText: 'Parameters',
            ...options,
        };

        /*eslint-disable no-caller,no-restricted-properties*/
        const scopeFn = arguments.callee.caller;
        const scopeArgsNames = /\(([^)]*)/.exec(scopeFn)[1].split(',');
        const scopeArgsValues = Array.prototype.slice.call(scopeFn.arguments);
        /*eslint-enable no-caller,no-restricted-properties*/

        if (options.group) {
            if (options.groupCollapsed) {
                this.groupCollapsed(options.groupText);
            } else {
                this.group(options.groupText);
            }
        }

        scopeArgsValues.forEach((value, index) => {
            if (index < scopeArgsNames.length) {
                this.log(`${scopeArgsNames[index].trim()} =`, value);
            } else {
                this.warn(`${scopeArgsNames[index].trim()} =`, value, '[unassigned]');
            }
        });

        if (options.group) {
            this.groupEnd();
        }
    }

    /**
     * Set current level.
     * @param level
     */
    setLevel(level) {
        this.level = level;
    }
}
