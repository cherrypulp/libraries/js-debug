/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/Debug.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/@babel/runtime/helpers/arrayWithoutHoles.js":
/*!******************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/arrayWithoutHoles.js ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _arrayWithoutHoles(arr) {
  if (Array.isArray(arr)) {
    for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) {
      arr2[i] = arr[i];
    }

    return arr2;
  }
}

module.exports = _arrayWithoutHoles;

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/classCallCheck.js":
/*!***************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/classCallCheck.js ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

module.exports = _classCallCheck;

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/createClass.js":
/*!************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/createClass.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

module.exports = _createClass;

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/defineProperty.js":
/*!***************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/defineProperty.js ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

module.exports = _defineProperty;

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/iterableToArray.js":
/*!****************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/iterableToArray.js ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _iterableToArray(iter) {
  if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter);
}

module.exports = _iterableToArray;

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/nonIterableSpread.js":
/*!******************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/nonIterableSpread.js ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _nonIterableSpread() {
  throw new TypeError("Invalid attempt to spread non-iterable instance");
}

module.exports = _nonIterableSpread;

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/toConsumableArray.js":
/*!******************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/toConsumableArray.js ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var arrayWithoutHoles = __webpack_require__(/*! ./arrayWithoutHoles */ "./node_modules/@babel/runtime/helpers/arrayWithoutHoles.js");

var iterableToArray = __webpack_require__(/*! ./iterableToArray */ "./node_modules/@babel/runtime/helpers/iterableToArray.js");

var nonIterableSpread = __webpack_require__(/*! ./nonIterableSpread */ "./node_modules/@babel/runtime/helpers/nonIterableSpread.js");

function _toConsumableArray(arr) {
  return arrayWithoutHoles(arr) || iterableToArray(arr) || nonIterableSpread();
}

module.exports = _toConsumableArray;

/***/ }),

/***/ "./src/Debug.js":
/*!**********************!*\
  !*** ./src/Debug.js ***!
  \**********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Debug; });
/* harmony import */ var _babel_runtime_helpers_toConsumableArray__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/toConsumableArray */ "./node_modules/@babel/runtime/helpers/toConsumableArray.js");
/* harmony import */ var _babel_runtime_helpers_toConsumableArray__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_toConsumableArray__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/classCallCheck */ "./node_modules/@babel/runtime/helpers/classCallCheck.js");
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime/helpers/createClass */ "./node_modules/@babel/runtime/helpers/createClass.js");
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime/helpers/defineProperty */ "./node_modules/@babel/runtime/helpers/defineProperty.js");
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _helpers__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./helpers */ "./src/helpers.js");
var cov_wyvc3j7a4 = function () {
  var path = "/Volumes/Documents/Works/js-debug/src/Debug.js";
  var hash = "c3fae9b0438faee4f925c2df5656b7bd4947ce0f";
  var global = new Function("return this")();
  var gcv = "__coverage__";
  var coverageData = {
    path: "/Volumes/Documents/Works/js-debug/src/Debug.js",
    statementMap: {
      "0": {
        start: {
          line: 11,
          column: 23
        },
        end: {
          line: 11,
          column: 25
        }
      },
      "1": {
        start: {
          line: 17,
          column: 20
        },
        end: {
          line: 45,
          column: 5
        }
      },
      "2": {
        start: {
          line: 53,
          column: 8
        },
        end: {
          line: 55,
          column: 9
        }
      },
      "3": {
        start: {
          line: 54,
          column: 12
        },
        end: {
          line: 54,
          column: 48
        }
      },
      "4": {
        start: {
          line: 57,
          column: 8
        },
        end: {
          line: 57,
          column: 37
        }
      },
      "5": {
        start: {
          line: 64,
          column: 8
        },
        end: {
          line: 68,
          column: 10
        }
      },
      "6": {
        start: {
          line: 70,
          column: 8
        },
        end: {
          line: 70,
          column: 26
        }
      },
      "7": {
        start: {
          line: 71,
          column: 8
        },
        end: {
          line: 71,
          column: 35
        }
      },
      "8": {
        start: {
          line: 72,
          column: 8
        },
        end: {
          line: 72,
          column: 37
        }
      },
      "9": {
        start: {
          line: 75,
          column: 8
        },
        end: {
          line: 90,
          column: 15
        }
      },
      "10": {
        start: {
          line: 76,
          column: 36
        },
        end: {
          line: 76,
          column: 61
        }
      },
      "11": {
        start: {
          line: 78,
          column: 16
        },
        end: {
          line: 89,
          column: 18
        }
      },
      "12": {
        start: {
          line: 79,
          column: 20
        },
        end: {
          line: 81,
          column: 21
        }
      },
      "13": {
        start: {
          line: 80,
          column: 24
        },
        end: {
          line: 80,
          column: 31
        }
      },
      "14": {
        start: {
          line: 83,
          column: 20
        },
        end: {
          line: 83,
          column: 47
        }
      },
      "15": {
        start: {
          line: 84,
          column: 20
        },
        end: {
          line: 87,
          column: 23
        }
      },
      "16": {
        start: {
          line: 88,
          column: 20
        },
        end: {
          line: 88,
          column: 49
        }
      },
      "17": {
        start: {
          line: 97,
          column: 8
        },
        end: {
          line: 99,
          column: 9
        }
      },
      "18": {
        start: {
          line: 98,
          column: 12
        },
        end: {
          line: 98,
          column: 19
        }
      },
      "19": {
        start: {
          line: 101,
          column: 8
        },
        end: {
          line: 101,
          column: 35
        }
      },
      "20": {
        start: {
          line: 102,
          column: 8
        },
        end: {
          line: 102,
          column: 43
        }
      },
      "21": {
        start: {
          line: 111,
          column: 8
        },
        end: {
          line: 113,
          column: 9
        }
      },
      "22": {
        start: {
          line: 112,
          column: 12
        },
        end: {
          line: 112,
          column: 25
        }
      },
      "23": {
        start: {
          line: 115,
          column: 24
        },
        end: {
          line: 115,
          column: 110
        }
      },
      "24": {
        start: {
          line: 115,
          column: 77
        },
        end: {
          line: 115,
          column: 109
        }
      },
      "25": {
        start: {
          line: 116,
          column: 8
        },
        end: {
          line: 116,
          column: 74
        }
      },
      "26": {
        start: {
          line: 129,
          column: 8
        },
        end: {
          line: 131,
          column: 9
        }
      },
      "27": {
        start: {
          line: 130,
          column: 12
        },
        end: {
          line: 130,
          column: 19
        }
      },
      "28": {
        start: {
          line: 133,
          column: 8
        },
        end: {
          line: 138,
          column: 10
        }
      },
      "29": {
        start: {
          line: 141,
          column: 24
        },
        end: {
          line: 141,
          column: 47
        }
      },
      "30": {
        start: {
          line: 142,
          column: 31
        },
        end: {
          line: 142,
          column: 70
        }
      },
      "31": {
        start: {
          line: 143,
          column: 32
        },
        end: {
          line: 143,
          column: 77
        }
      },
      "32": {
        start: {
          line: 146,
          column: 8
        },
        end: {
          line: 152,
          column: 9
        }
      },
      "33": {
        start: {
          line: 147,
          column: 12
        },
        end: {
          line: 151,
          column: 13
        }
      },
      "34": {
        start: {
          line: 148,
          column: 16
        },
        end: {
          line: 148,
          column: 55
        }
      },
      "35": {
        start: {
          line: 150,
          column: 16
        },
        end: {
          line: 150,
          column: 46
        }
      },
      "36": {
        start: {
          line: 154,
          column: 8
        },
        end: {
          line: 160,
          column: 11
        }
      },
      "37": {
        start: {
          line: 155,
          column: 12
        },
        end: {
          line: 159,
          column: 13
        }
      },
      "38": {
        start: {
          line: 156,
          column: 16
        },
        end: {
          line: 156,
          column: 69
        }
      },
      "39": {
        start: {
          line: 158,
          column: 16
        },
        end: {
          line: 158,
          column: 86
        }
      },
      "40": {
        start: {
          line: 162,
          column: 8
        },
        end: {
          line: 164,
          column: 9
        }
      },
      "41": {
        start: {
          line: 163,
          column: 12
        },
        end: {
          line: 163,
          column: 28
        }
      },
      "42": {
        start: {
          line: 172,
          column: 8
        },
        end: {
          line: 172,
          column: 27
        }
      },
      "43": {
        start: {
          line: 176,
          column: 0
        },
        end: {
          line: 176,
          column: 35
        }
      }
    },
    fnMap: {
      "0": {
        name: "(anonymous_0)",
        decl: {
          start: {
            line: 52,
            column: 4
          },
          end: {
            line: 52,
            column: 5
          }
        },
        loc: {
          start: {
            line: 52,
            column: 41
          },
          end: {
            line: 58,
            column: 5
          }
        },
        line: 52
      },
      "1": {
        name: "(anonymous_1)",
        decl: {
          start: {
            line: 63,
            column: 4
          },
          end: {
            line: 63,
            column: 5
          }
        },
        loc: {
          start: {
            line: 63,
            column: 30
          },
          end: {
            line: 91,
            column: 5
          }
        },
        line: 63
      },
      "2": {
        name: "(anonymous_2)",
        decl: {
          start: {
            line: 76,
            column: 20
          },
          end: {
            line: 76,
            column: 21
          }
        },
        loc: {
          start: {
            line: 76,
            column: 36
          },
          end: {
            line: 76,
            column: 61
          }
        },
        line: 76
      },
      "3": {
        name: "(anonymous_3)",
        decl: {
          start: {
            line: 77,
            column: 21
          },
          end: {
            line: 77,
            column: 22
          }
        },
        loc: {
          start: {
            line: 77,
            column: 33
          },
          end: {
            line: 90,
            column: 13
          }
        },
        line: 77
      },
      "4": {
        name: "(anonymous_4)",
        decl: {
          start: {
            line: 78,
            column: 31
          },
          end: {
            line: 78,
            column: 32
          }
        },
        loc: {
          start: {
            line: 78,
            column: 44
          },
          end: {
            line: 89,
            column: 17
          }
        },
        line: 78
      },
      "5": {
        name: "(anonymous_5)",
        decl: {
          start: {
            line: 96,
            column: 4
          },
          end: {
            line: 96,
            column: 5
          }
        },
        loc: {
          start: {
            line: 96,
            column: 24
          },
          end: {
            line: 103,
            column: 5
          }
        },
        line: 96
      },
      "6": {
        name: "(anonymous_6)",
        decl: {
          start: {
            line: 110,
            column: 4
          },
          end: {
            line: 110,
            column: 5
          }
        },
        loc: {
          start: {
            line: 110,
            column: 24
          },
          end: {
            line: 117,
            column: 5
          }
        },
        line: 110
      },
      "7": {
        name: "(anonymous_7)",
        decl: {
          start: {
            line: 115,
            column: 68
          },
          end: {
            line: 115,
            column: 69
          }
        },
        loc: {
          start: {
            line: 115,
            column: 77
          },
          end: {
            line: 115,
            column: 109
          }
        },
        line: 115
      },
      "8": {
        name: "(anonymous_8)",
        decl: {
          start: {
            line: 128,
            column: 4
          },
          end: {
            line: 128,
            column: 5
          }
        },
        loc: {
          start: {
            line: 128,
            column: 25
          },
          end: {
            line: 165,
            column: 5
          }
        },
        line: 128
      },
      "9": {
        name: "(anonymous_9)",
        decl: {
          start: {
            line: 154,
            column: 32
          },
          end: {
            line: 154,
            column: 33
          }
        },
        loc: {
          start: {
            line: 154,
            column: 50
          },
          end: {
            line: 160,
            column: 9
          }
        },
        line: 154
      },
      "10": {
        name: "(anonymous_10)",
        decl: {
          start: {
            line: 171,
            column: 4
          },
          end: {
            line: 171,
            column: 5
          }
        },
        loc: {
          start: {
            line: 171,
            column: 20
          },
          end: {
            line: 173,
            column: 5
          }
        },
        line: 171
      }
    },
    branchMap: {
      "0": {
        loc: {
          start: {
            line: 52,
            column: 23
          },
          end: {
            line: 52,
            column: 39
          }
        },
        type: "default-arg",
        locations: [{
          start: {
            line: 52,
            column: 30
          },
          end: {
            line: 52,
            column: 39
          }
        }],
        line: 52
      },
      "1": {
        loc: {
          start: {
            line: 53,
            column: 8
          },
          end: {
            line: 55,
            column: 9
          }
        },
        type: "if",
        locations: [{
          start: {
            line: 53,
            column: 8
          },
          end: {
            line: 55,
            column: 9
          }
        }, {
          start: {
            line: 53,
            column: 8
          },
          end: {
            line: 55,
            column: 9
          }
        }],
        line: 53
      },
      "2": {
        loc: {
          start: {
            line: 63,
            column: 16
          },
          end: {
            line: 63,
            column: 28
          }
        },
        type: "default-arg",
        locations: [{
          start: {
            line: 63,
            column: 26
          },
          end: {
            line: 63,
            column: 28
          }
        }],
        line: 63
      },
      "3": {
        loc: {
          start: {
            line: 79,
            column: 20
          },
          end: {
            line: 81,
            column: 21
          }
        },
        type: "if",
        locations: [{
          start: {
            line: 79,
            column: 20
          },
          end: {
            line: 81,
            column: 21
          }
        }, {
          start: {
            line: 79,
            column: 20
          },
          end: {
            line: 81,
            column: 21
          }
        }],
        line: 79
      },
      "4": {
        loc: {
          start: {
            line: 97,
            column: 8
          },
          end: {
            line: 99,
            column: 9
          }
        },
        type: "if",
        locations: [{
          start: {
            line: 97,
            column: 8
          },
          end: {
            line: 99,
            column: 9
          }
        }, {
          start: {
            line: 97,
            column: 8
          },
          end: {
            line: 99,
            column: 9
          }
        }],
        line: 97
      },
      "5": {
        loc: {
          start: {
            line: 111,
            column: 8
          },
          end: {
            line: 113,
            column: 9
          }
        },
        type: "if",
        locations: [{
          start: {
            line: 111,
            column: 8
          },
          end: {
            line: 113,
            column: 9
          }
        }, {
          start: {
            line: 111,
            column: 8
          },
          end: {
            line: 113,
            column: 9
          }
        }],
        line: 111
      },
      "6": {
        loc: {
          start: {
            line: 128,
            column: 11
          },
          end: {
            line: 128,
            column: 23
          }
        },
        type: "default-arg",
        locations: [{
          start: {
            line: 128,
            column: 21
          },
          end: {
            line: 128,
            column: 23
          }
        }],
        line: 128
      },
      "7": {
        loc: {
          start: {
            line: 129,
            column: 8
          },
          end: {
            line: 131,
            column: 9
          }
        },
        type: "if",
        locations: [{
          start: {
            line: 129,
            column: 8
          },
          end: {
            line: 131,
            column: 9
          }
        }, {
          start: {
            line: 129,
            column: 8
          },
          end: {
            line: 131,
            column: 9
          }
        }],
        line: 129
      },
      "8": {
        loc: {
          start: {
            line: 146,
            column: 8
          },
          end: {
            line: 152,
            column: 9
          }
        },
        type: "if",
        locations: [{
          start: {
            line: 146,
            column: 8
          },
          end: {
            line: 152,
            column: 9
          }
        }, {
          start: {
            line: 146,
            column: 8
          },
          end: {
            line: 152,
            column: 9
          }
        }],
        line: 146
      },
      "9": {
        loc: {
          start: {
            line: 147,
            column: 12
          },
          end: {
            line: 151,
            column: 13
          }
        },
        type: "if",
        locations: [{
          start: {
            line: 147,
            column: 12
          },
          end: {
            line: 151,
            column: 13
          }
        }, {
          start: {
            line: 147,
            column: 12
          },
          end: {
            line: 151,
            column: 13
          }
        }],
        line: 147
      },
      "10": {
        loc: {
          start: {
            line: 155,
            column: 12
          },
          end: {
            line: 159,
            column: 13
          }
        },
        type: "if",
        locations: [{
          start: {
            line: 155,
            column: 12
          },
          end: {
            line: 159,
            column: 13
          }
        }, {
          start: {
            line: 155,
            column: 12
          },
          end: {
            line: 159,
            column: 13
          }
        }],
        line: 155
      },
      "11": {
        loc: {
          start: {
            line: 162,
            column: 8
          },
          end: {
            line: 164,
            column: 9
          }
        },
        type: "if",
        locations: [{
          start: {
            line: 162,
            column: 8
          },
          end: {
            line: 164,
            column: 9
          }
        }, {
          start: {
            line: 162,
            column: 8
          },
          end: {
            line: 164,
            column: 9
          }
        }],
        line: 162
      }
    },
    s: {
      "0": 0,
      "1": 0,
      "2": 0,
      "3": 0,
      "4": 0,
      "5": 0,
      "6": 0,
      "7": 0,
      "8": 0,
      "9": 0,
      "10": 0,
      "11": 0,
      "12": 0,
      "13": 0,
      "14": 0,
      "15": 0,
      "16": 0,
      "17": 0,
      "18": 0,
      "19": 0,
      "20": 0,
      "21": 0,
      "22": 0,
      "23": 0,
      "24": 0,
      "25": 0,
      "26": 0,
      "27": 0,
      "28": 0,
      "29": 0,
      "30": 0,
      "31": 0,
      "32": 0,
      "33": 0,
      "34": 0,
      "35": 0,
      "36": 0,
      "37": 0,
      "38": 0,
      "39": 0,
      "40": 0,
      "41": 0,
      "42": 0,
      "43": 0
    },
    f: {
      "0": 0,
      "1": 0,
      "2": 0,
      "3": 0,
      "4": 0,
      "5": 0,
      "6": 0,
      "7": 0,
      "8": 0,
      "9": 0,
      "10": 0
    },
    b: {
      "0": [0],
      "1": [0, 0],
      "2": [0],
      "3": [0, 0],
      "4": [0, 0],
      "5": [0, 0],
      "6": [0],
      "7": [0, 0],
      "8": [0, 0],
      "9": [0, 0],
      "10": [0, 0],
      "11": [0, 0]
    },
    _coverageSchema: "43e27e138ebf9cfc5966b082cf9a028302ed4184",
    hash: "c3fae9b0438faee4f925c2df5656b7bd4947ce0f"
  };
  var coverage = global[gcv] || (global[gcv] = {});

  if (coverage[path] && coverage[path].hash === hash) {
    return coverage[path];
  }

  return coverage[path] = coverageData;
}();






function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_3___default()(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }



var Debug = function () {
  _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_2___default()(Debug, null, [{
    key: "getInstance",
    value: function getInstance() {
      var name = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : (cov_wyvc3j7a4.b[0][0]++, 'default');
      cov_wyvc3j7a4.f[0]++;
      cov_wyvc3j7a4.s[2]++;

      if (typeof Debug.instances[name] === 'undefined') {
        cov_wyvc3j7a4.b[1][0]++;
        cov_wyvc3j7a4.s[3]++;
        Debug.instances[name] = new Debug();
      } else {
        cov_wyvc3j7a4.b[1][1]++;
      }

      cov_wyvc3j7a4.s[4]++;
      return Debug.instances[name];
    }
  }]);

  function Debug() {
    var _this = this;

    var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : (cov_wyvc3j7a4.b[2][0]++, {});

    _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1___default()(this, Debug);

    cov_wyvc3j7a4.f[1]++;
    cov_wyvc3j7a4.s[5]++;
    options = _objectSpread({
      level: 'off',
      logger: console
    }, options);
    cov_wyvc3j7a4.s[6]++;
    this.history = [];
    cov_wyvc3j7a4.s[7]++;
    this.level = options.level;
    cov_wyvc3j7a4.s[8]++;
    this.logger = options.logger;
    cov_wyvc3j7a4.s[9]++;
    Object.values(this.constructor.levels).reduce(function (acc, level) {
      cov_wyvc3j7a4.f[2]++;
      cov_wyvc3j7a4.s[10]++;
      return acc.concat(level.methods);
    }, []).forEach(function (method) {
      cov_wyvc3j7a4.f[3]++;
      cov_wyvc3j7a4.s[11]++;

      _this[method] = function () {
        var _this$logger;

        for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
          args[_key] = arguments[_key];
        }

        cov_wyvc3j7a4.f[4]++;
        cov_wyvc3j7a4.s[12]++;

        if (!_this.isAllowedTo(method)) {
          cov_wyvc3j7a4.b[3][0]++;
          cov_wyvc3j7a4.s[13]++;
          return;
        } else {
          cov_wyvc3j7a4.b[3][1]++;
        }

        cov_wyvc3j7a4.s[14]++;
        args = _helpers__WEBPACK_IMPORTED_MODULE_4__["formatArgs"].apply(void 0, _babel_runtime_helpers_toConsumableArray__WEBPACK_IMPORTED_MODULE_0___default()(args));
        cov_wyvc3j7a4.s[15]++;

        _this.history.push({
          method: method,
          args: args
        });

        cov_wyvc3j7a4.s[16]++;

        (_this$logger = _this.logger)[method].apply(_this$logger, _babel_runtime_helpers_toConsumableArray__WEBPACK_IMPORTED_MODULE_0___default()(args));
      };
    });
  }

  _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_2___default()(Debug, [{
    key: "deprecated",
    value: function deprecated() {
      for (var _len2 = arguments.length, args = new Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
        args[_key2] = arguments[_key2];
      }

      cov_wyvc3j7a4.f[5]++;
      cov_wyvc3j7a4.s[17]++;

      if (!this.isAllowedTo('warn')) {
        cov_wyvc3j7a4.b[4][0]++;
        cov_wyvc3j7a4.s[18]++;
        return;
      } else {
        cov_wyvc3j7a4.b[4][1]++;
      }

      cov_wyvc3j7a4.s[19]++;
      args = _helpers__WEBPACK_IMPORTED_MODULE_4__["formatArgs"].apply(void 0, _babel_runtime_helpers_toConsumableArray__WEBPACK_IMPORTED_MODULE_0___default()(args));
      cov_wyvc3j7a4.s[20]++;
      this.warn.apply(this, ['[deprecated]'].concat(_babel_runtime_helpers_toConsumableArray__WEBPACK_IMPORTED_MODULE_0___default()(args)));
    }
  }, {
    key: "isAllowedTo",
    value: function isAllowedTo(fnName) {
      cov_wyvc3j7a4.f[6]++;
      cov_wyvc3j7a4.s[21]++;

      if (this.level === 'off') {
        cov_wyvc3j7a4.b[5][0]++;
        cov_wyvc3j7a4.s[22]++;
        return false;
      } else {
        cov_wyvc3j7a4.b[5][1]++;
      }

      var fnLevel = (cov_wyvc3j7a4.s[23]++, Object.values(this.constructor.levels).find(function (lvl) {
        cov_wyvc3j7a4.f[7]++;
        cov_wyvc3j7a4.s[24]++;
        return lvl.methods.indexOf(fnName) > -1;
      }));
      cov_wyvc3j7a4.s[25]++;
      return fnLevel.level <= this.constructor.levels[this.level].level;
    }
  }, {
    key: "params",
    value: function params() {
      var _this2 = this;

      var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : (cov_wyvc3j7a4.b[6][0]++, {});
      cov_wyvc3j7a4.f[8]++;
      cov_wyvc3j7a4.s[26]++;

      if (!this.isAllowedTo('log')) {
        cov_wyvc3j7a4.b[7][0]++;
        cov_wyvc3j7a4.s[27]++;
        return;
      } else {
        cov_wyvc3j7a4.b[7][1]++;
      }

      cov_wyvc3j7a4.s[28]++;
      options = _objectSpread({
        group: true,
        groupCollapsed: true,
        groupText: 'Parameters'
      }, options);
      var scopeFn = (cov_wyvc3j7a4.s[29]++, arguments.callee.caller);
      var scopeArgsNames = (cov_wyvc3j7a4.s[30]++, /\(([^)]*)/.exec(scopeFn)[1].split(','));
      var scopeArgsValues = (cov_wyvc3j7a4.s[31]++, Array.prototype.slice.call(scopeFn.arguments));
      cov_wyvc3j7a4.s[32]++;

      if (options.group) {
        cov_wyvc3j7a4.b[8][0]++;
        cov_wyvc3j7a4.s[33]++;

        if (options.groupCollapsed) {
          cov_wyvc3j7a4.b[9][0]++;
          cov_wyvc3j7a4.s[34]++;
          this.groupCollapsed(options.groupText);
        } else {
          cov_wyvc3j7a4.b[9][1]++;
          cov_wyvc3j7a4.s[35]++;
          this.group(options.groupText);
        }
      } else {
        cov_wyvc3j7a4.b[8][1]++;
      }

      cov_wyvc3j7a4.s[36]++;
      scopeArgsValues.forEach(function (value, index) {
        cov_wyvc3j7a4.f[9]++;
        cov_wyvc3j7a4.s[37]++;

        if (index < scopeArgsNames.length) {
          cov_wyvc3j7a4.b[10][0]++;
          cov_wyvc3j7a4.s[38]++;

          _this2.log("".concat(scopeArgsNames[index].trim(), " ="), value);
        } else {
          cov_wyvc3j7a4.b[10][1]++;
          cov_wyvc3j7a4.s[39]++;

          _this2.warn("".concat(scopeArgsNames[index].trim(), " ="), value, '[unassigned]');
        }
      });
      cov_wyvc3j7a4.s[40]++;

      if (options.group) {
        cov_wyvc3j7a4.b[11][0]++;
        cov_wyvc3j7a4.s[41]++;
        this.groupEnd();
      } else {
        cov_wyvc3j7a4.b[11][1]++;
      }
    }
  }, {
    key: "setLevel",
    value: function setLevel(level) {
      cov_wyvc3j7a4.f[10]++;
      cov_wyvc3j7a4.s[42]++;
      this.level = level;
    }
  }]);

  return Debug;
}();

_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_3___default()(Debug, "instances", (cov_wyvc3j7a4.s[0]++, {}));

_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_3___default()(Debug, "levels", (cov_wyvc3j7a4.s[1]++, {
  off: {
    level: 0,
    methods: []
  },
  exception: {
    level: 1,
    methods: ['exception']
  },
  error: {
    level: 2,
    methods: ['error', 'group', 'groupCollapsed', 'groupEnd']
  },
  warn: {
    level: 4,
    methods: ['warn']
  },
  info: {
    level: 8,
    methods: ['info']
  },
  log: {
    level: 16,
    methods: ['log', 'debug', 'time', 'timeEnd', 'profile', 'profileEnd', 'count', 'trace', 'dir', 'dirxml', 'assert', 'table']
  }
}));


cov_wyvc3j7a4.s[43]++;
window.debug = Debug.getInstance();

/***/ }),

/***/ "./src/helpers.js":
/*!************************!*\
  !*** ./src/helpers.js ***!
  \************************/
/*! exports provided: formats, formatArgs, formatString, hasStyleSupport */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "formats", function() { return formats; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "formatArgs", function() { return formatArgs; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "formatString", function() { return formatString; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "hasStyleSupport", function() { return hasStyleSupport; });
var cov_17aepmoa3l = function () {
  var path = "/Volumes/Documents/Works/js-debug/src/helpers.js";
  var hash = "e1a33d62a5c3833c1e22a74702959729dc8e35bb";
  var global = new Function("return this")();
  var gcv = "__coverage__";
  var coverageData = {
    path: "/Volumes/Documents/Works/js-debug/src/helpers.js",
    statementMap: {
      "0": {
        start: {
          line: 1,
          column: 23
        },
        end: {
          line: 62,
          column: 1
        }
      },
      "1": {
        start: {
          line: 6,
          column: 12
        },
        end: {
          line: 6,
          column: 43
        }
      },
      "2": {
        start: {
          line: 9,
          column: 12
        },
        end: {
          line: 9,
          column: 98
        }
      },
      "3": {
        start: {
          line: 16,
          column: 12
        },
        end: {
          line: 16,
          column: 31
        }
      },
      "4": {
        start: {
          line: 19,
          column: 12
        },
        end: {
          line: 19,
          column: 58
        }
      },
      "5": {
        start: {
          line: 26,
          column: 12
        },
        end: {
          line: 26,
          column: 37
        }
      },
      "6": {
        start: {
          line: 29,
          column: 12
        },
        end: {
          line: 29,
          column: 46
        }
      },
      "7": {
        start: {
          line: 36,
          column: 12
        },
        end: {
          line: 36,
          column: 37
        }
      },
      "8": {
        start: {
          line: 39,
          column: 12
        },
        end: {
          line: 39,
          column: 47
        }
      },
      "9": {
        start: {
          line: 46,
          column: 12
        },
        end: {
          line: 46,
          column: 31
        }
      },
      "10": {
        start: {
          line: 49,
          column: 12
        },
        end: {
          line: 49,
          column: 118
        }
      },
      "11": {
        start: {
          line: 56,
          column: 12
        },
        end: {
          line: 56,
          column: 31
        }
      },
      "12": {
        start: {
          line: 59,
          column: 12
        },
        end: {
          line: 59,
          column: 114
        }
      },
      "13": {
        start: {
          line: 71,
          column: 4
        },
        end: {
          line: 73,
          column: 5
        }
      },
      "14": {
        start: {
          line: 72,
          column: 8
        },
        end: {
          line: 72,
          column: 20
        }
      },
      "15": {
        start: {
          line: 75,
          column: 18
        },
        end: {
          line: 75,
          column: 20
        }
      },
      "16": {
        start: {
          line: 76,
          column: 19
        },
        end: {
          line: 76,
          column: 21
        }
      },
      "17": {
        start: {
          line: 78,
          column: 4
        },
        end: {
          line: 94,
          column: 11
        }
      },
      "18": {
        start: {
          line: 80,
          column: 12
        },
        end: {
          line: 93,
          column: 13
        }
      },
      "19": {
        start: {
          line: 81,
          column: 31
        },
        end: {
          line: 81,
          column: 48
        }
      },
      "20": {
        start: {
          line: 83,
          column: 16
        },
        end: {
          line: 89,
          column: 17
        }
      },
      "21": {
        start: {
          line: 84,
          column: 20
        },
        end: {
          line: 84,
          column: 46
        }
      },
      "22": {
        start: {
          line: 85,
          column: 20
        },
        end: {
          line: 85,
          column: 66
        }
      },
      "23": {
        start: {
          line: 85,
          column: 46
        },
        end: {
          line: 85,
          column: 64
        }
      },
      "24": {
        start: {
          line: 87,
          column: 20
        },
        end: {
          line: 87,
          column: 37
        }
      },
      "25": {
        start: {
          line: 88,
          column: 20
        },
        end: {
          line: 88,
          column: 37
        }
      },
      "26": {
        start: {
          line: 91,
          column: 16
        },
        end: {
          line: 91,
          column: 33
        }
      },
      "27": {
        start: {
          line: 92,
          column: 16
        },
        end: {
          line: 92,
          column: 33
        }
      },
      "28": {
        start: {
          line: 96,
          column: 4
        },
        end: {
          line: 96,
          column: 43
        }
      },
      "29": {
        start: {
          line: 105,
          column: 4
        },
        end: {
          line: 107,
          column: 5
        }
      },
      "30": {
        start: {
          line: 106,
          column: 8
        },
        end: {
          line: 106,
          column: 19
        }
      },
      "31": {
        start: {
          line: 109,
          column: 22
        },
        end: {
          line: 109,
          column: 24
        }
      },
      "32": {
        start: {
          line: 111,
          column: 4
        },
        end: {
          line: 123,
          column: 7
        }
      },
      "33": {
        start: {
          line: 112,
          column: 8
        },
        end: {
          line: 122,
          column: 9
        }
      },
      "34": {
        start: {
          line: 113,
          column: 26
        },
        end: {
          line: 113,
          column: 49
        }
      },
      "35": {
        start: {
          line: 115,
          column: 12
        },
        end: {
          line: 121,
          column: 13
        }
      },
      "36": {
        start: {
          line: 116,
          column: 16
        },
        end: {
          line: 116,
          column: 65
        }
      },
      "37": {
        start: {
          line: 117,
          column: 16
        },
        end: {
          line: 120,
          column: 19
        }
      },
      "38": {
        start: {
          line: 125,
          column: 19
        },
        end: {
          line: 127,
          column: 63
        }
      },
      "39": {
        start: {
          line: 126,
          column: 24
        },
        end: {
          line: 126,
          column: 41
        }
      },
      "40": {
        start: {
          line: 127,
          column: 33
        },
        end: {
          line: 127,
          column: 58
        }
      },
      "41": {
        start: {
          line: 129,
          column: 4
        },
        end: {
          line: 129,
          column: 32
        }
      },
      "42": {
        start: {
          line: 138,
          column: 4
        },
        end: {
          line: 138,
          column: 16
        }
      }
    },
    fnMap: {
      "0": {
        name: "(anonymous_0)",
        decl: {
          start: {
            line: 5,
            column: 8
          },
          end: {
            line: 5,
            column: 9
          }
        },
        loc: {
          start: {
            line: 5,
            column: 19
          },
          end: {
            line: 7,
            column: 9
          }
        },
        line: 5
      },
      "1": {
        name: "(anonymous_1)",
        decl: {
          start: {
            line: 8,
            column: 8
          },
          end: {
            line: 8,
            column: 9
          }
        },
        loc: {
          start: {
            line: 8,
            column: 17
          },
          end: {
            line: 10,
            column: 9
          }
        },
        line: 8
      },
      "2": {
        name: "(anonymous_2)",
        decl: {
          start: {
            line: 15,
            column: 8
          },
          end: {
            line: 15,
            column: 9
          }
        },
        loc: {
          start: {
            line: 15,
            column: 24
          },
          end: {
            line: 17,
            column: 9
          }
        },
        line: 15
      },
      "3": {
        name: "(anonymous_3)",
        decl: {
          start: {
            line: 18,
            column: 8
          },
          end: {
            line: 18,
            column: 9
          }
        },
        loc: {
          start: {
            line: 18,
            column: 17
          },
          end: {
            line: 20,
            column: 9
          }
        },
        line: 18
      },
      "4": {
        name: "(anonymous_4)",
        decl: {
          start: {
            line: 25,
            column: 8
          },
          end: {
            line: 25,
            column: 9
          }
        },
        loc: {
          start: {
            line: 25,
            column: 28
          },
          end: {
            line: 27,
            column: 9
          }
        },
        line: 25
      },
      "5": {
        name: "(anonymous_5)",
        decl: {
          start: {
            line: 28,
            column: 8
          },
          end: {
            line: 28,
            column: 9
          }
        },
        loc: {
          start: {
            line: 28,
            column: 17
          },
          end: {
            line: 30,
            column: 9
          }
        },
        line: 28
      },
      "6": {
        name: "(anonymous_6)",
        decl: {
          start: {
            line: 35,
            column: 8
          },
          end: {
            line: 35,
            column: 9
          }
        },
        loc: {
          start: {
            line: 35,
            column: 28
          },
          end: {
            line: 37,
            column: 9
          }
        },
        line: 35
      },
      "7": {
        name: "(anonymous_7)",
        decl: {
          start: {
            line: 38,
            column: 8
          },
          end: {
            line: 38,
            column: 9
          }
        },
        loc: {
          start: {
            line: 38,
            column: 17
          },
          end: {
            line: 40,
            column: 9
          }
        },
        line: 38
      },
      "8": {
        name: "(anonymous_8)",
        decl: {
          start: {
            line: 45,
            column: 8
          },
          end: {
            line: 45,
            column: 9
          }
        },
        loc: {
          start: {
            line: 45,
            column: 24
          },
          end: {
            line: 47,
            column: 9
          }
        },
        line: 45
      },
      "9": {
        name: "(anonymous_9)",
        decl: {
          start: {
            line: 48,
            column: 8
          },
          end: {
            line: 48,
            column: 9
          }
        },
        loc: {
          start: {
            line: 48,
            column: 17
          },
          end: {
            line: 50,
            column: 9
          }
        },
        line: 48
      },
      "10": {
        name: "(anonymous_10)",
        decl: {
          start: {
            line: 55,
            column: 8
          },
          end: {
            line: 55,
            column: 9
          }
        },
        loc: {
          start: {
            line: 55,
            column: 24
          },
          end: {
            line: 57,
            column: 9
          }
        },
        line: 55
      },
      "11": {
        name: "(anonymous_11)",
        decl: {
          start: {
            line: 58,
            column: 8
          },
          end: {
            line: 58,
            column: 9
          }
        },
        loc: {
          start: {
            line: 58,
            column: 17
          },
          end: {
            line: 60,
            column: 9
          }
        },
        line: 58
      },
      "12": {
        name: "formatArgs",
        decl: {
          start: {
            line: 70,
            column: 16
          },
          end: {
            line: 70,
            column: 26
          }
        },
        loc: {
          start: {
            line: 70,
            column: 36
          },
          end: {
            line: 97,
            column: 1
          }
        },
        line: 70
      },
      "13": {
        name: "(anonymous_13)",
        decl: {
          start: {
            line: 79,
            column: 17
          },
          end: {
            line: 79,
            column: 18
          }
        },
        loc: {
          start: {
            line: 79,
            column: 26
          },
          end: {
            line: 94,
            column: 9
          }
        },
        line: 79
      },
      "14": {
        name: "(anonymous_14)",
        decl: {
          start: {
            line: 85,
            column: 35
          },
          end: {
            line: 85,
            column: 36
          }
        },
        loc: {
          start: {
            line: 85,
            column: 46
          },
          end: {
            line: 85,
            column: 64
          }
        },
        line: 85
      },
      "15": {
        name: "formatString",
        decl: {
          start: {
            line: 104,
            column: 16
          },
          end: {
            line: 104,
            column: 28
          }
        },
        loc: {
          start: {
            line: 104,
            column: 34
          },
          end: {
            line: 130,
            column: 1
          }
        },
        line: 104
      },
      "16": {
        name: "(anonymous_16)",
        decl: {
          start: {
            line: 111,
            column: 20
          },
          end: {
            line: 111,
            column: 21
          }
        },
        loc: {
          start: {
            line: 111,
            column: 32
          },
          end: {
            line: 123,
            column: 5
          }
        },
        line: 111
      },
      "17": {
        name: "(anonymous_17)",
        decl: {
          start: {
            line: 126,
            column: 14
          },
          end: {
            line: 126,
            column: 15
          }
        },
        loc: {
          start: {
            line: 126,
            column: 24
          },
          end: {
            line: 126,
            column: 41
          }
        },
        line: 126
      },
      "18": {
        name: "(anonymous_18)",
        decl: {
          start: {
            line: 127,
            column: 16
          },
          end: {
            line: 127,
            column: 17
          }
        },
        loc: {
          start: {
            line: 127,
            column: 33
          },
          end: {
            line: 127,
            column: 58
          }
        },
        line: 127
      },
      "19": {
        name: "hasStyleSupport",
        decl: {
          start: {
            line: 136,
            column: 16
          },
          end: {
            line: 136,
            column: 31
          }
        },
        loc: {
          start: {
            line: 136,
            column: 34
          },
          end: {
            line: 139,
            column: 1
          }
        },
        line: 136
      }
    },
    branchMap: {
      "0": {
        loc: {
          start: {
            line: 26,
            column: 24
          },
          end: {
            line: 26,
            column: 32
          }
        },
        type: "binary-expr",
        locations: [{
          start: {
            line: 26,
            column: 24
          },
          end: {
            line: 26,
            column: 26
          }
        }, {
          start: {
            line: 26,
            column: 30
          },
          end: {
            line: 26,
            column: 32
          }
        }],
        line: 26
      },
      "1": {
        loc: {
          start: {
            line: 36,
            column: 24
          },
          end: {
            line: 36,
            column: 32
          }
        },
        type: "binary-expr",
        locations: [{
          start: {
            line: 36,
            column: 24
          },
          end: {
            line: 36,
            column: 26
          }
        }, {
          start: {
            line: 36,
            column: 30
          },
          end: {
            line: 36,
            column: 32
          }
        }],
        line: 36
      },
      "2": {
        loc: {
          start: {
            line: 71,
            column: 4
          },
          end: {
            line: 73,
            column: 5
          }
        },
        type: "if",
        locations: [{
          start: {
            line: 71,
            column: 4
          },
          end: {
            line: 73,
            column: 5
          }
        }, {
          start: {
            line: 71,
            column: 4
          },
          end: {
            line: 73,
            column: 5
          }
        }],
        line: 71
      },
      "3": {
        loc: {
          start: {
            line: 80,
            column: 12
          },
          end: {
            line: 93,
            column: 13
          }
        },
        type: "if",
        locations: [{
          start: {
            line: 80,
            column: 12
          },
          end: {
            line: 93,
            column: 13
          }
        }, {
          start: {
            line: 80,
            column: 12
          },
          end: {
            line: 93,
            column: 13
          }
        }],
        line: 80
      },
      "4": {
        loc: {
          start: {
            line: 83,
            column: 16
          },
          end: {
            line: 89,
            column: 17
          }
        },
        type: "if",
        locations: [{
          start: {
            line: 83,
            column: 16
          },
          end: {
            line: 89,
            column: 17
          }
        }, {
          start: {
            line: 83,
            column: 16
          },
          end: {
            line: 89,
            column: 17
          }
        }],
        line: 83
      },
      "5": {
        loc: {
          start: {
            line: 105,
            column: 4
          },
          end: {
            line: 107,
            column: 5
          }
        },
        type: "if",
        locations: [{
          start: {
            line: 105,
            column: 4
          },
          end: {
            line: 107,
            column: 5
          }
        }, {
          start: {
            line: 105,
            column: 4
          },
          end: {
            line: 107,
            column: 5
          }
        }],
        line: 105
      },
      "6": {
        loc: {
          start: {
            line: 115,
            column: 12
          },
          end: {
            line: 121,
            column: 13
          }
        },
        type: "if",
        locations: [{
          start: {
            line: 115,
            column: 12
          },
          end: {
            line: 121,
            column: 13
          }
        }, {
          start: {
            line: 115,
            column: 12
          },
          end: {
            line: 121,
            column: 13
          }
        }],
        line: 115
      }
    },
    s: {
      "0": 0,
      "1": 0,
      "2": 0,
      "3": 0,
      "4": 0,
      "5": 0,
      "6": 0,
      "7": 0,
      "8": 0,
      "9": 0,
      "10": 0,
      "11": 0,
      "12": 0,
      "13": 0,
      "14": 0,
      "15": 0,
      "16": 0,
      "17": 0,
      "18": 0,
      "19": 0,
      "20": 0,
      "21": 0,
      "22": 0,
      "23": 0,
      "24": 0,
      "25": 0,
      "26": 0,
      "27": 0,
      "28": 0,
      "29": 0,
      "30": 0,
      "31": 0,
      "32": 0,
      "33": 0,
      "34": 0,
      "35": 0,
      "36": 0,
      "37": 0,
      "38": 0,
      "39": 0,
      "40": 0,
      "41": 0,
      "42": 0
    },
    f: {
      "0": 0,
      "1": 0,
      "2": 0,
      "3": 0,
      "4": 0,
      "5": 0,
      "6": 0,
      "7": 0,
      "8": 0,
      "9": 0,
      "10": 0,
      "11": 0,
      "12": 0,
      "13": 0,
      "14": 0,
      "15": 0,
      "16": 0,
      "17": 0,
      "18": 0,
      "19": 0
    },
    b: {
      "0": [0, 0],
      "1": [0, 0],
      "2": [0, 0],
      "3": [0, 0],
      "4": [0, 0],
      "5": [0, 0],
      "6": [0, 0]
    },
    _coverageSchema: "43e27e138ebf9cfc5966b082cf9a028302ed4184",
    hash: "e1a33d62a5c3833c1e22a74702959729dc8e35bb"
  };
  var coverage = global[gcv] || (global[gcv] = {});

  if (coverage[path] && coverage[path].hash === hash) {
    return coverage[path];
  }

  return coverage[path] = coverageData;
}();

var formats = (cov_17aepmoa3l.s[0]++, [{
  regex: /---.*|\**\*.*/,
  replacer: function replacer() {
    cov_17aepmoa3l.f[0]++;
    cov_17aepmoa3l.s[1]++;
    return "%c".concat('-'.repeat(80), "%c");
  },
  styles: function styles() {
    cov_17aepmoa3l.f[1]++;
    cov_17aepmoa3l.s[2]++;
    return ['background: #e1e4e8; color: #e1e4e8; line-height: 4px; margin: 20px 0;', ''];
  }
}, {
  regex: /~~([^~~]+)~~/,
  replacer: function replacer(m, p1) {
    cov_17aepmoa3l.f[2]++;
    cov_17aepmoa3l.s[3]++;
    return "%c".concat(p1, "%c");
  },
  styles: function styles() {
    cov_17aepmoa3l.f[3]++;
    cov_17aepmoa3l.s[4]++;
    return ['text-decoration: line-through;', ''];
  }
}, {
  regex: /\*\*([^**]+)\*\*|__([^__]+)__/,
  replacer: function replacer(m, p1, p2) {
    cov_17aepmoa3l.f[4]++;
    cov_17aepmoa3l.s[5]++;
    return "%c".concat((cov_17aepmoa3l.b[0][0]++, p1) || (cov_17aepmoa3l.b[0][1]++, p2), "%c");
  },
  styles: function styles() {
    cov_17aepmoa3l.f[5]++;
    cov_17aepmoa3l.s[6]++;
    return ['font-weight: bold;', ''];
  }
}, {
  regex: /\*([^*]+)\*|_([^_]+)_/,
  replacer: function replacer(m, p1, p2) {
    cov_17aepmoa3l.f[6]++;
    cov_17aepmoa3l.s[7]++;
    return "%c".concat((cov_17aepmoa3l.b[1][0]++, p1) || (cov_17aepmoa3l.b[1][1]++, p2), "%c");
  },
  styles: function styles() {
    cov_17aepmoa3l.f[7]++;
    cov_17aepmoa3l.s[8]++;
    return ['font-style: italic;', ''];
  }
}, {
  regex: /`([^`]+)`/,
  replacer: function replacer(m, p1) {
    cov_17aepmoa3l.f[8]++;
    cov_17aepmoa3l.s[9]++;
    return "%c".concat(p1, "%c");
  },
  styles: function styles() {
    cov_17aepmoa3l.f[9]++;
    cov_17aepmoa3l.s[10]++;
    return ['background: rgba(27, 31, 35, .05); font-size: 85%; padding: .2em .4em; border-radius: 3px;', ''];
  }
}, {
  regex: /^>(.*)$/,
  replacer: function replacer(m, p1) {
    cov_17aepmoa3l.f[10]++;
    cov_17aepmoa3l.s[11]++;
    return "%c".concat(p1, "%c");
  },
  styles: function styles() {
    cov_17aepmoa3l.f[11]++;
    cov_17aepmoa3l.s[12]++;
    return ['background: #f6f8fa; border-left: .25em solid #dfe2e5; color: #6a737d; padding: 0 1em;', ''];
  }
}]);
function formatArgs() {
  cov_17aepmoa3l.f[12]++;
  cov_17aepmoa3l.s[13]++;

  for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
    args[_key] = arguments[_key];
  }

  if (!hasStyleSupport()) {
    cov_17aepmoa3l.b[2][0]++;
    cov_17aepmoa3l.s[14]++;
    return args;
  } else {
    cov_17aepmoa3l.b[2][1]++;
  }

  var message = (cov_17aepmoa3l.s[15]++, '');
  var params = (cov_17aepmoa3l.s[16]++, []);
  cov_17aepmoa3l.s[17]++;
  args.forEach(function (arg) {
    cov_17aepmoa3l.f[13]++;
    cov_17aepmoa3l.s[18]++;

    if (typeof arg === 'string') {
      cov_17aepmoa3l.b[3][0]++;
      var values = (cov_17aepmoa3l.s[19]++, formatString(arg));
      cov_17aepmoa3l.s[20]++;

      if (Array.isArray(values)) {
        cov_17aepmoa3l.b[4][0]++;
        cov_17aepmoa3l.s[21]++;
        message += values.shift();
        cov_17aepmoa3l.s[22]++;
        values.forEach(function (value) {
          cov_17aepmoa3l.f[14]++;
          cov_17aepmoa3l.s[23]++;
          return params.push(value);
        });
      } else {
        cov_17aepmoa3l.b[4][1]++;
        cov_17aepmoa3l.s[24]++;
        message += '%s ';
        cov_17aepmoa3l.s[25]++;
        params.push(arg);
      }
    } else {
      cov_17aepmoa3l.b[3][1]++;
      cov_17aepmoa3l.s[26]++;
      message += '%o ';
      cov_17aepmoa3l.s[27]++;
      params.push(arg);
    }
  });
  cov_17aepmoa3l.s[28]++;
  return [message.trim()].concat(params);
}
function formatString(arg) {
  cov_17aepmoa3l.f[15]++;
  cov_17aepmoa3l.s[29]++;

  if (typeof arg !== 'string') {
    cov_17aepmoa3l.b[5][0]++;
    cov_17aepmoa3l.s[30]++;
    return arg;
  } else {
    cov_17aepmoa3l.b[5][1]++;
  }

  var formatted = (cov_17aepmoa3l.s[31]++, []);
  cov_17aepmoa3l.s[32]++;
  formats.forEach(function (format) {
    cov_17aepmoa3l.f[16]++;
    cov_17aepmoa3l.s[33]++;

    while (arg.match(format.regex)) {
      var match = (cov_17aepmoa3l.s[34]++, arg.match(format.regex));
      cov_17aepmoa3l.s[35]++;

      if (match) {
        cov_17aepmoa3l.b[6][0]++;
        cov_17aepmoa3l.s[36]++;
        arg = arg.replace(format.regex, format.replacer);
        cov_17aepmoa3l.s[37]++;
        formatted.push({
          index: match.index,
          styles: format.styles(match)
        });
      } else {
        cov_17aepmoa3l.b[6][1]++;
      }
    }
  });
  var styles = (cov_17aepmoa3l.s[38]++, formatted.sort(function (a, b) {
    cov_17aepmoa3l.f[17]++;
    cov_17aepmoa3l.s[39]++;
    return a.index - b.index;
  }).reduce(function (acc, format) {
    cov_17aepmoa3l.f[18]++;
    cov_17aepmoa3l.s[40]++;
    return acc.concat(format.styles);
  }, []));
  cov_17aepmoa3l.s[41]++;
  return [arg].concat(styles);
}
function hasStyleSupport() {
  cov_17aepmoa3l.f[19]++;
  cov_17aepmoa3l.s[42]++;
  return true;
}

/***/ })

/******/ });
//# sourceMappingURL=Debug.js.map